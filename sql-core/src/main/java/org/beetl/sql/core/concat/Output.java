package org.beetl.sql.core.concat;

public interface Output {
    void toSql(ConcatBuilder sb);
}
